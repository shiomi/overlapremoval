#include "../include/TriggerLateStudy.h"
#include "/home/shiomi/RootUtils/RootUtils/TLegend_addfunc.h"
#include "/home/shiomi/RootUtils/RootUtils/TCanvas_opt.h"
#include "TStyle.h"
#include "TF1.h"
#include "THStack.h"
#include "TLegend.h"

using namespace std;

void TriggerLateStudy::Draw(TString pdf)
{
    TCanvas_opt *c1 = new TCanvas_opt();
    gStyle->SetOptStat(0);
    c1->SetTopMargin(0.20);
    c1->Print(pdf + "[", "pdf");

    O_etaphi->Draw("colz");
    c1->Print(pdf,"pdf");

    N_etaphi->Draw("colz");
    c1->Print(pdf,"pdf");

    h_etaphi->Draw("colz");
    c1->Print(pdf,"pdf");

    c1->Clear();

    A_eta->Draw("hist");
    A_eta->SetFillColor(kOrange+10);
    O_eta->Draw("same hist");
    O_eta->SetFillColor(kOrange);
    N_eta->Draw("same hist");
    N_eta->SetFillColor(kGreen+2);
    c1->Print(pdf,"pdf");

    cout << A_eta->Integral() << endl;
    cout << O_eta->Integral() << endl;
    cout << N_eta->Integral() << endl;

    c1 -> Print( pdf + "]", "pdf" );
    delete c1;
}
