#include "../include/TriggerLateStudy.h"
#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <iostream>
#include <vector>
#include <stdlib.h>
#include <typeinfo>
#include <TMath.h>

void TriggerLateStudy::InitHist()
{
    A_eta = new TH1D("A_eta",";#eta^{L1};Events",75,-2.5,2.5);
    O_eta = new TH1D("O_eta",";#eta^{L1};Events",75,-2.5,2.5);
    N_eta = new TH1D("N_eta",";#eta^{L1};Events",75,-2.5,2.5);
    h_etaphi = new TH2D("h_etaphi",";#eta^{L1};#phi^{L1}",100,-2.5,2.5,96,-TMath::Pi(),TMath::Pi());
    O_etaphi = new TH2D("O_etaphi",";#eta^{L1};#phi^{L1}",100,-2.5,2.5,96,-TMath::Pi(),TMath::Pi());
    N_etaphi = new TH2D("N_etaphi",";#eta^{L1};#phi^{L1}",100,-2.5,2.5,96,-TMath::Pi(),TMath::Pi());
}

void TriggerLateStudy::End()
{
    if(A_eta!=0){delete A_eta;}
    if(O_eta!=0){delete O_eta;}
    if(N_eta!=0){delete N_eta;}
    if(h_etaphi!=0){delete h_etaphi;}
    if(O_etaphi!=0){delete O_etaphi;}
    if(N_etaphi!=0){delete N_etaphi;}
}
