#include "../include/TriggerLateStudy.h"
#include <iostream>
#include <vector>
#include <stdlib.h>
#include <typeinfo>

using namespace std;

void TriggerLateStudy::FillHist()
{
    Clear();
    bool hlt = HLT();
    if(hlt){
        if(muctpi_ndatawords>=1){RPC_Run3();}
        if(roi_eta.size()>=2){
            for(unsigned int i=0;i!=roi_eta.size();i++){
                O_etaphi->Fill(roi_eta.at(i),roi_phi.at(i),(float)1/(float)roi_eta.size());
                A_eta->Fill(roi_eta.at(i),(float)1/(float)roi_eta.size());
            }
            OverlapRemoval();
            int count = 0;
            int Ncount = 0;
            for(unsigned int i=0;i!=roi_eta.size();i++){
                if(roi_ovlp.at(i)!=1){ count = count+1; }
                if(roi_ovlp.at(i)!=1 && roi_ovlp.at(i)!=2){ Ncount = Ncount+1; }
            }
            if(count>=2){
                for(unsigned int i=0;i!=roi_eta.size();i++){
                    if(roi_ovlp.at(i)!=1){O_eta->Fill(roi_eta.at(i),(float)1/(float)count);}
                }
            }
            if(Ncount>=2){
                for(unsigned int i=0;i!=roi_eta.size();i++){
                    if(roi_ovlp.at(i)!=1 && roi_ovlp.at(i)!=2){
                        N_etaphi->Fill(roi_eta.at(i),roi_phi.at(i),(float)1/(float)Ncount);
                        N_eta->Fill(roi_eta.at(i),(float)1/(float)Ncount);
                    }
                }
            }
        }
    }
}

void TriggerLateStudy::Clear()
{
    roi_eta.clear();
    roi_phi.clear();
    roi_roi.clear();
    roi_sector.clear();
    roi_source.clear();
    roi_side.clear();
    roi_ovlp.clear();
}
